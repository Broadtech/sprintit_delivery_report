# -*- coding: utf-8 -*-
##############################################################################
#
#    ODOO Open Source Management Solution
#
#    ODOO Addon module by Sprintit Ltd
#    Copyright (C) 2015 Sprintit Ltd (<http://sprintit.fi>).
#
##############################################################################
{
    'name': 'Sprintit Delivery Report',
    'version': '1.2',
    'category': 'Sprintit Delivery Report',
    'description': """Delivery Note Report""",
    'author': 'SprintIT',
    'website': 'http://www.sprintit.fi',
    'images': [],
    'depends': ['sprintit_mbo_customization', 'stock'],
    'data': [
        'res_partner_view.xml',
        'report.xml',
        'views/report_delivery_note.xml',
        ],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
    'application': True,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: